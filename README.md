# Proj0-Hello
-------------

Trivial project to exercise version control, turn-in, and other
mechanisms.

## Info:
---------------
Author: River Veek
Email: riverv@uoregon.edu
Description: Project 0 consists of the hello.py file that will print "Hello World." 
after being run. To run the file, type 'make run' into the prompt.

